<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', 'API\V1\AuthController@login')->name('login');
Route::post('/register', 'API\V1\AuthController@register')->name('register');

// Route::group(['prefix' => 'admin'], function () {
//     // auth
//     Route::post('/login', 'API\V1\Admin\LoginController@login')->name('admin.login');
//     Route::get('/get-admin','API\V1\Admin\LoginController@getAdmin')->name('admin.get');
//     Route::group(['middleware' => 'auth:sanctum'], function () {
//         Route::get('/get-admin','API\V1\Admin\LoginController@getAdmin')->name('admin.get');
//     });
//     // brand
//     Route::get('/brand/all','API\V1\Admin\BrandController@index')->name('brand.all');
//     Route::post('/brand/create', 'API\V1\Admin\BrandController@create')->name('brand.create');
//     Route::get('/brand/show/{id}','API\V1\Admin\BrandController@show')->name('brand.show');
//     Route::post('/brand/update', 'API\V1\Admin\BrandController@update')->name('brand.update');
//     Route::post('/brand/delete', 'API\V1\Admin\BrandController@delete')->name('brand.delete');

//     // color
//     Route::get('/color/all','API\V1\Admin\ColorController@index')->name('color.all');
//     Route::post('/color/create', 'API\V1\Admin\ColorController@create')->name('color.create');
//     Route::post('/color/update', 'API\V1\Admin\ColorController@update')->name('color.update');
//     Route::post('/color/delete', 'API\V1\Admin\ColorController@delete')->name('color.delete');

//     // size
//     Route::get('/size/all','API\V1\Admin\SizeController@index')->name('size.all');
//     Route::post('/size/create', 'API\V1\Admin\SizeController@create')->name('size.create');
//     Route::post('/size/update', 'API\V1\Admin\SizeController@update')->name('size.update');
//     Route::post('/size/delete', 'API\V1\Admin\SizeController@delete')->name('size.delete');

//      // shirt
//      Route::get('/shirt/all','API\V1\Admin\ShirtController@index')->name('shirt.all');
//      Route::post('/shirt/create', 'API\V1\Admin\ShirtController@create')->name('shirt.create');
//      Route::post('/shirt/update', 'API\V1\Admin\ShirtController@update')->name('shirt.update');
//      Route::post('/shirt/delete', 'API\V1\Admin\ShirtController@delete')->name('shirt.delete');
// });
// Route::middleware('auth:sanctum')->get('/get-user', function (Request $request) {
//     return $request->user();
// });


Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/get-user', 'API\V1\AuthController@getUser');
});