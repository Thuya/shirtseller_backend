<?php

use App\Notifications\MessageSent;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     Notification::route('smspoh', '09250801213')->notify(new MessageSent('hello world!'));
//     return view('welcome');
// });

Route::prefix('admin')->group(function() {
    // auth
    Route::post('/login', 'API\V1\Admin\LoginController@login')->name('admin.login');

    // Route::middleware(['auth:admin'], function () {
        Route::get('/get-admin', 'API\V1\Admin\LoginController@getAdmin')->name('admin.get'); 
        Route::post('/logout', 'API\V1\Admin\LoginController@logout')->name('admin.logout');
        Route::post('/profile/update', 'API\V1\Admin\LoginController@update')->name('admin.update'); 

        // brand
        Route::get('/brand/all','API\V1\Admin\BrandController@index')->name('brand.all');
        Route::post('/brand/create', 'API\V1\Admin\BrandController@create')->name('brand.create');
        Route::get('/brand/show/{id}','API\V1\Admin\BrandController@show')->name('brand.show');
        Route::post('/brand/update', 'API\V1\Admin\BrandController@update')->name('brand.update');
        Route::post('/brand/delete', 'API\V1\Admin\BrandController@delete')->name('brand.delete');
        
        // color
        Route::get('/color/all','API\V1\Admin\ColorController@index')->name('color.all');
        Route::post('/color/create', 'API\V1\Admin\ColorController@create')->name('color.create');
        Route::get('/color/show/{id}','API\V1\Admin\ColorController@show')->name('color.show');
        Route::post('/color/update', 'API\V1\Admin\ColorController@update')->name('color.update');
        Route::post('/color/delete', 'API\V1\Admin\ColorController@delete')->name('color.delete');
        
        // size
        Route::get('/size/all','API\V1\Admin\SizeController@index')->name('size.all');
        Route::post('/size/create', 'API\V1\Admin\SizeController@create')->name('size.create');
        Route::get('/size/show/{id}','API\V1\Admin\SizeController@show')->name('size.show');
        Route::post('/size/update', 'API\V1\Admin\SizeController@update')->name('size.update');
        Route::post('/size/delete', 'API\V1\Admin\SizeController@delete')->name('size.delete');
        
        // shirt
        Route::get('/shirt/all','API\V1\Admin\ShirtController@index')->name('shirt.all');
        Route::get('/shirt/get-req-data','API\V1\Admin\ShirtController@reqData')->name('shirt.req');
        Route::post('/shirt/create', 'API\V1\Admin\ShirtController@create')->name('shirt.create');
        Route::get('/shirt/show/{id}','API\V1\Admin\ShirtController@show')->name('shirt.show');
        Route::post('/shirt/update', 'API\V1\Admin\ShirtController@update')->name('shirt.update');
        Route::post('/shirt/delete', 'API\V1\Admin\ShirtController@delete')->name('shirt.delete');
        Route::post('/shirts/status/change', 'API\V1\Admin\ShirtController@changeStatus')->name('shirt.changeStatus');

        // variant
        Route::get('/shirt/variant/all/{id}','API\V1\Admin\VariantController@index')->name('variant.all');
        Route::get('/shirts/variant/create/{id}', 'API\V1\Admin\VariantController@create')->name('variant.create');
        Route::post('/shirts/variant/insert', 'API\V1\Admin\VariantController@insert')->name('variant.insert');
        Route::get('/shirts/variant/edit/{id}', 'API\V1\Admin\VariantController@edit')->name('variant.edit');
        Route::post('/shirts/variant/update', 'API\V1\Admin\VariantController@update')->name('variant.update');
        Route::post('/shirts/variant/delete', 'API\V1\Admin\VariantController@delete')->name('variant.delete');
    // });
});

Route::get('404', function () {
    abort(404);
});

Route::get('/admin', function () {
    return view('welcome');
})->where('any', '.*');

Route::get('/admin/{any}', function () {
    return view('welcome');
})->where('any', '.*');
