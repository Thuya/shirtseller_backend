<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use App\Model\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with('orderDetails')->get();

        return response()->json([
            'orders' => $orders
        ]);
    }

    public function show($id)
    {
        $order = Order::with('orderDetails')->where('id', $id)->first();

        return response()->json([
            'order' => $order
        ]);
    }

    public function delete(Request $request)
    {
        $order = Order::findorFail($request->id);

        $order->delete();

        return response()->json([
            'message' => 'success'
        ], 200);
    }
}
