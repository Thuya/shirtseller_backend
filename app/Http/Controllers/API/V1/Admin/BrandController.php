<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Services\CreateOrUpdateBrand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->keywords)) {
            
            $brands = Brand::where('name', 'LIKE', '%' . $request->keywords . '%')
                        ->orderBy('id', 'desc')
                        ->paginate(20);

        } else {
            $brands = Brand::orderBy('id', 'desc')->paginate(20);
        }
        
        return response()->json([
            'brands' => $brands
        ]);
    }

    public function create(Request $request)
    {
        $this->check($request);

        CreateOrUpdateBrand::make($request);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function show($id)
    {
        $brand = Brand::where('id', $id)->first();
        
        return response()->json([
            'brand' => $brand
        ]);
    }

    public function update(Request $request)
    {
        $this->check($request);

        CreateOrUpdateBrand::make($request);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function delete(Request $request)
    {
        $brand = Brand::findorFail($request->id);

        $brand->delete();

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    protected function check($request)
    {
        $request->validate([
            'status' => 'bail|required|min:1|max:2',
            'name' => 'bail|required',
        ]);
    }
}
