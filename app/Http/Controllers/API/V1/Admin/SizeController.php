<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use App\Model\Size;
use App\Services\CreateOrUpdateSize;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->keywords)) { 
            $sizes = Size::where('name', 'LIKE', '%' . $request->keywords . '%')
                ->orderBy('id', 'desc')
                ->paginate(20);
        } else {
            $sizes = Size::orderBy('id', 'desc')->paginate();
        }
        
        return response()->json([
            'sizes' => $sizes
        ]);
    }

    public function create(Request $request)
    {
        $this->check($request);

        CreateOrUpdateSize::make($request);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function show($id)
    {
        $size = Size::where('id', $id)->first();
        
        return response()->json([
            'size' => $size
        ]);
    }

    public function update(Request $request)
    {
        $this->check($request);
        
        CreateOrUpdateSize::make($request);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function delete(Request $request)
    {
        $size = Size::findorFail($request->id);

        $size->delete();

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    protected function check($request)
    {
        $request->validate([
            'status' => 'bail|required|min:1|max:2',
            'name' => 'bail|required',
        ]);
    }
}
