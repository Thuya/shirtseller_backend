<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'bail|required|email',
            'password' => 'bail|required',
        ]);

        $auth = Auth::guard('admin')
            ->attempt([
                'email' => $request->email, 
                'password' => $request->password
            ]);

        if ($auth) {
            return response()->json([ 'auth' => $auth ], 200);
        } else {
            return response()->json([ 'auth' => $auth ], 400);
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return response()->json([ 'auth' => false ], 200);
    }

    public function getAdmin()
    {
        if (auth()->guard('admin')->check()) {
            // dd(Auth::guard('admin')->user());
            return response()->json([
                'auth' => true,
                'profile' => Auth::guard('admin')->user()
            ]);
        } else {
            return response()->json([
                'auth' => false,
                'profile' => null
            ]);
        } 
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'bail|required',
            'email' => 'bail|required|email',
            'old_password' => 'bail|required_with:new_password',
            'new_password' => 'bail|required_with:old_password',
        ]);

        $admin = Admin::findorFail($request->id);

        if ($request->has('new_password') && $request->has('old_password')) {

            if( !Hash::check($request->old_password, $admin->password) ) {
                return response()->json([
                    'errors' => [
                        'message' => 'Old password must be correct!' 
                    ]
                ], 422);
            }

            $admin->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->new_password),
            ]);
        } else {
            $admin->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);
        }

        return response()->json([
            'status' => 'success'
        ], 200);
    }
}
