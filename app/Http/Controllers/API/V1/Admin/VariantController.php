<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use App\Model\Shirt;
use App\Model\Size;
use App\Model\Variant;
use Illuminate\Http\Request;

class VariantController extends Controller
{
    public function index($id)
    {
        $variants = Variant::with('color', 'size')
            ->where('shirt_id', $id)
            ->get();

        return response()->json([
            'variants' => $variants
        ]);
    }

    public function create($id)
    {
        $shirt = Shirt::findorFail($id);

        if ($shirt) {
            
            $shirt = Shirt::with('colors')->where('id', $shirt->id)->first();
            $sizes = Size::where('status', 2)->get();

            return response()->json([
                'shirt' => $shirt,
                'sizes' => $sizes
            ], 200);

        } else {

            return abort(404);

        }
        
    }

    public function insert(Request $request)
    {
        $this->check($request);

        $existData = Variant::where([
            ['shirt_id', $request->shirt_id],
            ['color_id', $request->color_id],
            ['size_id', $request->size_id]
        ])->first();

        if ($existData) {
            return response()->json([
                'duplicate' => 'Your Data is already saved in the Database. Try another info!'
            ], 422);
        }

        Variant::create([
            'shirt_id' => $request->shirt_id,
            'color_id' => $request->color_id,
            'size_id' => $request->size_id,
            'quantity' => $request->quantity,
        ]);

        return response()->json([
            'status' => 'success'
        ], 200);
    }

    public function update(Request $request)
    {
        $this->check($request);
        
        $variant = Variant::findorFail($request->id);
        $variant->update([
            'shirt_id' => $request->shirt_id,
            'color_id' => $request->color_id,
            'size_id' => $request->size_id,
            'quantity' => $request->quantity,
        ]);

        return response()->json([
            'status' => 'success'
        ], 200);
    }

    public function delete(Request $request)
    {
        $variant = Variant::findorFail($request->id);
        $variant->delete();

        return response()->json([
            'status' => 'success'
        ], 200);
    }

    protected function check($request)
    {
        $request->validate([
            'shirt_id' => 'bail|required',
            'size_id' => 'bail|required',
            'color_id' => 'bail|required',
            'quantity' => 'bail|required|min:1',
        ]);
    }
}
