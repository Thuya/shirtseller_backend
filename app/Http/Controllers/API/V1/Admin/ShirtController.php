<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\ShirtCollection;
use App\Model\Brand;
use App\Model\Color;
use App\Model\Shirt;
use App\Model\ShirtImage;
use App\Services\CreateOrUpdateShirt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ShirtController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('keywords')) {

            $shirts = ShirtCollection::collection(
                    Shirt::with('colors', 'brand')->where('name', 'LIKE', '%' . $request->keywords . '%')
                        ->orderBy('id', 'desc')->paginate(10)
            );

        } else {

            $shirts = ShirtCollection::collection(
                    Shirt::with('colors', 'brand')->orderBy('id', 'desc')->paginate(10)
            );

        }
        

        return $shirts;
    }

    public function create(Request $request)
    {
        $this->check($request);

        $shirt = CreateOrUpdateShirt::make($request);

        $colors = Color::find($request->colors);

        $shirt->colors()->attach($colors);
        
        if ($request->hasFile('files')) {
            
            foreach ($request->file('files') as $image) {

                $name = 'shirt_'.time().'_'.rand(1111,9999).'_'.$image->getClientOriginalName();
    
                Storage::disk('local')->putFileAs('upload/shirts', $image, $name);
    
                ShirtImage::create([
                    'name' => '/upload/shirts/'.$name,
                    'shirt_id' => $shirt->id
                ]);
            }

        }
        
        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function reqData()
    {
        $colors = Color::where('status', 2)->select('id', 'name', 'code')->get();
        $brands = Brand::where('status', 2)->select('id', 'name')->get();

        return response()->json([
            'colors' => $colors,
            'brands' => $brands,
        ], 200);
    }

    public function show($id)
    {
        $shirt = Shirt::where('id', $id)->first();
        
        return response()->json([
            'shirt' => $shirt
        ], 200);
    }

    public function update(Request $request)
    {
        $this->check($request);

        $shirt = CreateOrUpdateShirt::make($request);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function delete(Request $request)
    {
        $shirt = Shirt::findorFail($request->id);

        $shirt->delete();

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function changeStatus(Request $request)
    {
        $shirt = Shirt::findorFail($request->id);
        
        $shirt->update([
            'status' => $request->status
        ]);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    protected function check($request)
    {
        $request->validate([
            'name'  => 'bail|required', 
            'price'  => 'bail|required', 
            'description'  => 'bail|required', 
            'brand_id'  => 'bail|required',
            'colors'  => 'bail|required',
        ]);
    }
}
