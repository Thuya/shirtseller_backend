<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use App\Model\Color;
use App\Services\CreateOrUpdateColor;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->keywords)) {
            $colors = Color::where('name', 'LIKE', '%' . $request->keywords . '%')
                ->orderBy('id', 'desc')
                ->paginate(20);
        } else {
            $colors = Color::orderBy('id', 'desc')->paginate(20);
        }

        return response()->json([
            'colors' => $colors
        ]);
    }

    public function create(Request $request)
    {
        $this->check($request);

        CreateOrUpdateColor::make($request);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function show($id)
    {
        $color = Color::where('id', $id)->first();
        
        return response()->json([
            'color' => $color
        ]);
    }

    public function update(Request $request)
    {
        $this->check($request);
        
        CreateOrUpdateColor::make($request);

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    public function delete(Request $request)
    {
        $color = Color::findorFail($request->id);

        $color->delete();

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    protected function check($request)
    {
        $request->validate([
            'status' => 'bail|required|min:1|max:2',
            'code' => 'bail|required|string',
            'name' => 'bail|required',
        ]);
    }
}
