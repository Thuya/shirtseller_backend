<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->check('login', $request);

        $user = User::where('email', $request->email)->first();

        $res = $this->token_generate($user, $request);

        return response()->json([
            'res' => $res,
        ], 200);
    }

    public function register(Request $request)
    {
        $this->check('register', $request);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        
        $res = $this->token_generate($user, $request);

        return response()->json([
            'res' => $res
        ], 200);
    }

    public function getUser()
    {
        if (Auth::check()) {
            return response()->json(Auth::user());
        } else {
            return response()->json([
                'auth' => false,
            ]);
        }   
    }

    public function logout()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $user->tokens()->delete();

            return response()->json([
                'logout' => true,
            ], 200);
        }
    }

    protected function token_generate($user, $request)
    {
        if (!$user || Hash::check($user->password, $request->password)) {
            return response()->json([
                'message' => 'These credentials do not match our records.'
            ], 404);
        }

        $token = $user->createToken($request->email)->plainTextToken;

        $response = [
            'token' => $token,
            'user' => $user,
        ];

        return $response;
    }

    protected function check($type, $request)
    {
        if($type == 'login') {
            $request->validate([
                'email' => 'required|email',
                'password' => 'required'
            ]);
        } else {
            $request->validate([
                'name' => 'bail|required|max:100',
                'email' => 'bail|required|email|unique:users',
                'password' => 'bail|required',
                'confirm_password' => 'bail|required|same:password',
            ]);
        }
    }
}
