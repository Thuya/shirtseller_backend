<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $table = 'variants';
    protected $fillable = ['shirt_id', 'color_id', 'size_id', 'quantity'];

    public function shirt()
    {
        return $this->belongsTo('App\Model\Shirt', 'shirt_id');
    }

    public function color()
    {
        return $this->belongsTo('App\Model\Color', 'color_id');
    }

    public function colorNameCode()
    {
        return $this->belongsTo('App\Model\Color', 'color_id')->select(['code', 'name']);
    }
    
    public function size()
    {
        return $this->belongsTo('App\Model\Size', 'size_id');
    }
}
