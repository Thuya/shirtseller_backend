<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'status',
        'total_price',
        'payment_option',
        'payment_type',
        'user_id',
    ];

    public function orderDetails()
    {
        return $this->hasMany('App\Model\OrderDetail', 'order_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
