<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shirt extends Model
{
    protected $table = 'shirts';
    protected $fillable = ['status', 'name', 'price', 'description', 'brand_id'];
    
    public function brand()
    {
        return $this->belongsTo('App\Model\Brand', 'brand_id');
    }

    public function colors()
    {
        return $this->belongsToMany('App\Model\Color', 'color_shirts', 'color_id', 'shirt_id');
    }
    
    public function shirt_images()
    {
        return $this->hasMany('App\Model\ShirtImage');
    }
}
