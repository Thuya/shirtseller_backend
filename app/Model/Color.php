<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';
    protected $fillable = ['status', 'code', 'name'];

    public function shirts()
    {
        return $this->belongsToMany('App\Model\Shirt', 'color_shirts', 'color_id', 'shirt_id');
    }
}
