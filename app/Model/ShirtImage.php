<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShirtImage extends Model
{
    protected $table = 'shirt_images';
    protected $fillable = ['name', 'shirt_id'];

    public function shirt()
    {
        return $this->belongsTo('App\Model\Shirt', 'shirt_id');
    }
}
