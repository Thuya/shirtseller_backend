<?php

namespace App\Services;

use App\Model\Size;

class CreateOrUpdateSize
{
    public static function make($request)
    {
        $size = Size::updateOrCreate([
            'id' => $request->id
        ], [
            'status' => $request->status,
            'name' => $request->name
        ]);

        return $size;
    }
}
