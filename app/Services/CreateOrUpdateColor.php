<?php

namespace App\Services;

use App\Model\Color;

class CreateOrUpdateColor
{
    public static function make($request)
    {
        $color = Color::updateOrCreate([
            'id' => $request->id
        ], [
            'status' => $request->status,
            'code' => $request->code,
            'name' => $request->name
        ]);

        return $color;
    }
}
