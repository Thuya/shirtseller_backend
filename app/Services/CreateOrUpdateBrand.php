<?php

namespace App\Services;

use App\Model\Brand;

class CreateOrUpdateBrand
{
    public static function make($request)
    {
        $brand = Brand::updateOrCreate([
            'id' => $request->id
        ], [
            'status' => $request->status,
            'name' => $request->name
        ]);

        return $brand;
    }
}
