<?php

namespace App\Services;

use App\Model\Shirt;

class CreateOrUpdateShirt
{
    public static function make($request)
    {
        $shirt = Shirt::updateOrCreate([
            'id' => $request->id
        ], [
            'status' => $request->status ? $request->status : 1,
            'name'  => $request->name, 
            'price'  => $request->price, 
            'description'  => $request->description, 
            'brand_id'  => $request->brand_id
        ]);

        return $shirt;
    }
}
