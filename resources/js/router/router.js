import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const admin = () => import('../layouts/AdminLayout')
const home = () => import('../pages/Index')
const Login = () => import('../pages/auth/SignIn') 
const LogOut = () => import('../pages/auth/SignOut') 
const profileEdit = () => import('../pages/profile/Update')

// orders
const orderIndex = () => import('../pages/orders/Index')
const orderShow = () => import('../pages/orders/Show')

// brands
const brandCreate = () => import('../pages/brands/Create')
const brandEdit = () => import('../pages/brands/Edit')
const brandIndex = () => import('../pages/brands/Index')

// sizes
const sizeCreate = () => import('../pages/sizes/Create')
const sizeEdit = () => import('../pages/sizes/Edit')
const sizeIndex = () => import('../pages/sizes/Index')

// colors
const colorCreate = () => import('../pages/colors/Create')
const colorEdit = () => import('../pages/colors/Edit')
const colorIndex = () => import('../pages/colors/Index')

// shirts
const shirtCreate = () => import('../pages/shirts/Create')
const shirtEdit = () => import('../pages/shirts/Edit')
const shirtIndex = () => import('../pages/shirts/Index')

// variants
const variantCreate = () => import('../pages/shirts/variants/Create')
const variantEdit = () => import('../pages/shirts/variants/Edit')

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin',
            redirect: '/admin',
            component: admin,
            children: [
                {
                    path: '/admin',
                    component: home,
                    name: 'IndexPage',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/signin',
                    component: Login,
                    name: 'AdminLogin',
                    meta: {
                        requireVisitor: true
                    }
                },
                {
                    path: '/admin/signout',
                    component: LogOut,
                    name: 'AdminLogout',
                    meta: {
                        requireAuth: true
                    }
                },

                {
                    path: '/admin/brands',
                    component: brandIndex,
                    name: 'brandIndex',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/brands/register',
                    component: brandCreate,
                    name: 'brandCreate',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/brands/edit/:id',
                    component: brandEdit,
                    name: 'brandEdit',
                    meta: { 
                        requireAuth: true
                    }
                },


                {
                    path: '/admin/sizes',
                    component: sizeIndex,
                    name: 'sizeIndex',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/sizes/register',
                    component: sizeCreate,
                    name: 'sizeCreate',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/sizes/edit/:id',
                    component: sizeEdit,
                    name: 'sizeEdit',
                    meta: { 
                        requireAuth: true
                    }
                },


                {
                    path: '/admin/colors',
                    component: colorIndex,
                    name: 'colorIndex',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/colors/register',
                    component: colorCreate,
                    name: 'colorCreate',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/colors/edit/:id',
                    component: colorEdit,
                    name: 'colorEdit',
                    meta: { 
                        requireAuth: true
                    }
                },

                {
                    path: '/admin/orders',
                    component: orderIndex,
                    name: 'orderIndex',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/order/:id',
                    component: orderShow,
                    name: 'orderShow',
                    meta: { 
                        requireAuth: true
                    }
                },


                {
                    path: '/admin/shirts',
                    component: shirtIndex,
                    name: 'shirtIndex',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/shirts/register',
                    component: shirtCreate,
                    name: 'shirtCreate',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/shirts/edit/:id',
                    component: shirtEdit,
                    name: 'shirtEdit',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/shirts/varient/register/:id',
                    component: variantCreate,
                    name: 'variantCreate',
                    meta: { 
                        requireAuth: true
                    }
                },
                {
                    path: '/admin/shirts/varient/edit/:id',
                    component: variantEdit,
                    name: 'variantEdit',
                    meta: { 
                        requireAuth: true
                    }
                },

                {
                    path: '/admin/profile/edit',
                    component: profileEdit,
                    name: 'profileEdit',
                    meta: { 
                        requireAuth: true
                    }
                },
            ]
        }
    ]
})

router.beforeEach((to, from, next) => {
    const authenticated = localStorage.getItem('authenticated') && localStorage.getItem('authenticated') == 'true' ? true : false;

    if (to.matched.some(record => record.meta.requireAuth)) {                
        if (!authenticated) {
            next({
                path: '/admin/signin',
                query: { redirect: to.fullPath }
            });
        } else {
            if (to.matched.some(record => record.meta.requireVisitor)) {
                if( authenticated ) {
                    next({
                        path: '/admin',
                    });
                } else {
                    next();
                }
            }
        } 
    }

    next();
})

export default router;