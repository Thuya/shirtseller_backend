const state = {
    orders: null,
    order: null,
    orderLoading: false,
};

const mutations = {
    GET_ORDERS: (state, data) => {
        state.orders = data
    },
    GET_ORDER: (state, data) => {
        state.order = data
    },
    ORDER_LOADING: (state, data) => {
        state.orderLoading = data
    }
};

const actions = {
    getOrders: ({ state, commit }) => {
        commit('ORDER_LOADING', true)
        return new Promise((resolve, reject) => {
            axios.get('/admin/orders/all')
                .then(response => {
                    console.log(response.data);
                    commit('GET_ORDERS', response.data.orders)
                    commit('ORDER_LOADING', false)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    commit('ORDER_LOADING', false)
                    reject()
                })
        })
    },

    getOrder: ({ state, commit }, payload) => {
        return new Promise((resolve, reject) => {
            axios.get('/admin/orders/detail/' + payload.data)
                .then(response => {
                    console.log(response.data);
                    commit('GET_ORDER', response.data.order)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    reject()
                })
        })
    },

    deleteOrder: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)

        return new Promise((resolve, reject) => {
            axios.post('/admin/orders/delete', formdata)
                .then(response => {
                    console.log(response.data);
                    dispatch('getOrders')
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    reject()
                })
        })
    },
};

const getters = {
    getAdminOrders: state => state.orders,
    getAdminOrder: state => state.order,
    orderLoading: state => state.orderLoading,
};

export default {
    state,
    mutations,
    actions,
    getters
}