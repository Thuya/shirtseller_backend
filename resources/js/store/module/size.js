const state = {
    sizes: null,
    size: null,
    sizeErrors: null,
    sizeLoading: false
};

const mutations = {
    SIZE_ERRORS: (state, data) => {
        state.sizeErrors = data
    },
    GET_SIZES: (state, data) => {
        state.sizes = data
    },
    GET_SIZE: (state, data) => {
        state.size = data
    },
    SIZE_LOADING: (state, data) => {
        state.sizeLoading = data
    }
};

const actions = {
    createSize: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('status', payload.status)
        formdata.append('name', payload.name)

        return new Promise((resolve, reject) => {
            axios.post('/admin/size/create', formdata)
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('SIZE_ERRORS', error.response.data.errors)

                    reject()
                })
        })
    },

    updateSize: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)
        formdata.append('status', payload.status)
        formdata.append('name', payload.name)

        return new Promise((resolve, reject) => {
            axios.post('/admin/size/update', formdata)
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('SIZE_ERRORS', error.response.data.errors)

                    reject()
                })
        })
    },

    getSize: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get(`/admin/size/show/${payload.id}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_SIZE', response.data.size)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    getSizes: ({ state, commit }) => {

        commit('SIZE_LOADING', true)
        return new Promise((resolve, reject) => {
            axios.get('/admin/size/all')
                .then(response => {
                    console.log(response.data)
                    commit('GET_SIZES', response.data.sizes)
                    commit('SIZE_LOADING', false)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    commit('SIZE_LOADING', false)
                    reject()
                })
        })
    },

    searchSize: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get(`/admin/size/all?keywords=${payload.keywords}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_SIZES', response.data.sizes)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    deleteSize: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)

        return new Promise((resolve, reject) => {
            axios.post('/admin/size/delete', formdata)
                .then(response => {
                    console.log(response.data)
                    dispatch('getSizes')
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },
};

const getters = {
    sizeErrors: state => state.sizeErrors,
    getSizes: state => state.sizes,
    getSize: state => state.size,
    sizeLoading: state => state.sizeLoading
};

export default {
    state,
    mutations,
    actions,
    getters
}