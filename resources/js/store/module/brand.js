const state = {
    brands: null,
    brand: null,
    brandErrors: null,
    brandLoading: false
};

const mutations = {
    BRAND_ERRORS: (state, data) => {
        state.brandErrors = data
    },
    GET_BRANDS: (state, data) => {
        state.brands = data
    },
    GET_BRAND: (state, data) => {
        state.brand = data
    },
    BRAND_LOADING: (state, data) => {
        state.brandLoading = data
    }
};

const actions = {
    createBrand: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('status', payload.status)
        formdata.append('name', payload.name)

        return new Promise((resolve, reject) => {
            axios.post('/admin/brand/create', formdata)
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('BRAND_ERRORS', error.response.data.errors)

                    reject()
                })
        })
    },

    updateBrand: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)
        formdata.append('status', payload.status)
        formdata.append('name', payload.name)

        return new Promise((resolve, reject) => {
            axios.post('/admin/brand/update', formdata)
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('BRAND_ERRORS', error.response.data.errors)

                    reject()
                })
        })
    },

    getBrand: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get(`/admin/brand/show/${payload.id}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_BRAND', response.data.brand)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    getBrands: ({ state, commit }) => {

        commit('BRAND_LOADING', true)
        return new Promise((resolve, reject) => {
            axios.get('/admin/brand/all')
                .then(response => {
                    console.log(response.data)
                    commit('GET_BRANDS', response.data.brands)
                    commit('BRAND_LOADING', false)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    commit('BRAND_LOADING', false)
                    reject()
                })
        })
    },

    searchBrand: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get(`/admin/brand/all?keywords=${payload.keywords}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_BRANDS', response.data.brands)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    deleteBrand: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)

        return new Promise((resolve, reject) => {
            axios.post('/admin/brand/delete', formdata)
                .then(response => {
                    console.log(response.data)
                    dispatch('getBrands')
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },
};

const getters = {
    brandErrors: state => state.brandErrors,
    getBrands: state => state.brands,
    getBrand: state => state.brand,
    brandLoading: state => state.brandLoading
};

export default {
    state,
    mutations,
    actions,
    getters
}