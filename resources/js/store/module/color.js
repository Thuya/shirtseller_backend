const state = {
    colors: null,
    color: null,
    colorErrors: null,
    colorLoading: false
};

const mutations = {
    COLOR_ERRORS: (state, data) => {
        state.colorErrors = data
    },
    GET_COLORS: (state, data) => {
        state.colors = data
    },
    GET_COLOR: (state, data) => {
        state.color = data
    },
    COLOR_LOADING: (state, data) => {
        state.colorLoading = data
    }
};

const actions = {
    createColor: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('status', payload.status)
        formdata.append('code', payload.code)
        formdata.append('name', payload.name)

        return new Promise((resolve, reject) => {
            axios.post('/admin/color/create', formdata)
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('COLOR_ERRORS', error.response.data.errors)

                    reject()
                })
        })
    },

    updateColor: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)
        formdata.append('status', payload.status)
        formdata.append('code', payload.code)
        formdata.append('name', payload.name)

        return new Promise((resolve, reject) => {
            axios.post('/admin/color/update', formdata)
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('COLOR_ERRORS', error.response.data.colors)

                    reject()
                })
        })
    },

    getColor: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get(`/admin/color/show/${payload.id}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_COLOR', response.data.color)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    getColors: ({ state, commit }) => {

        commit('COLOR_LOADING', true)
        return new Promise((resolve, reject) => {
            axios.get('/admin/color/all')
                .then(response => {
                    console.log(response.data)
                    commit('GET_COLORS', response.data.colors)
                    commit('COLOR_LOADING', false)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    commit('COLOR_LOADING', false)
                    reject()
                })
        })
    },

    searchColor: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get(`/admin/color/all?keywords=${payload.keywords}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_COLORS', response.data.colors)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    deleteColor: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)

        return new Promise((resolve, reject) => {
            axios.post('/admin/color/delete', formdata)
                .then(response => {
                    console.log(response.data)
                    dispatch('getColors')
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },
};

const getters = {
    colorErrors: state => state.colorErrors,
    getColors: state => state.colors,
    getColor: state => state.color,
    colorLoading: state => state.colorLoading
};

export default {
    state,
    mutations,
    actions,
    getters
}