const state = {
    shirts: null,
    shirt: null,
    shirtErrors: null,
    shirtLoading: false,
    reqShirtData: null,
    reqVariantData: null,
    reqVariantColor: null,
    reqVariantSize: null,
    variants: null,
    variantErrors: null,
};

const mutations = {
    SHIRT_ERRORS: (state, data) => {
        state.shirtErrors = data
    },
    VARIANT_ERRORS: (state, data) => {
        state.variantErrors = data
    },
    GET_SHIRTS: (state, data) => {
        state.shirts = data
    },
    GET_SHIRT: (state, data) => {
        state.shirt = data
    },
    SHIRT_LOADING: (state, data) => {
        state.shirtLoading = data
    },
    REQ_SHIRT_DATA: (state, data) => {
        state.reqShirtData = data
    },
    GET_REQ_VARIANT_DATA: (state, data) => {
        state.reqVariantData = data
        state.reqVariantColor = data.shirt.colors
        state.reqVariantSize = data.sizes
    },
    GET_VARIATNS: (state, data) => {
        state.variants = data
    }
};

const actions = {
    createShirt: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('name', payload.name)
        formdata.append('price', payload.price)
        formdata.append('description', payload.description)
        formdata.append('brand_id', payload.brand_id)
        

        for (let i = 0; i < payload.colors.length; i++) {
            const color = payload.colors[i]
            formdata.append(`colors[${i}]`, color)
        }

        for (let i = 0; i < payload.images.length; i++) {
            const file = payload.images[i]
            formdata.append(`files[${i}]`, file)
        }
        
        return new Promise((resolve, reject) => {
            axios.post('/admin/shirt/create', formdata, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('SHIRT_ERRORS', error.response.data.errors)

                    reject()
                })
        })
    },

    updateShirt: ({ state, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)
        formdata.append('name', payload.name)
        formdata.append('price', payload.price)
        formdata.append('description', payload.description)
        formdata.append('brand_id', payload.brand)

        return new Promise((resolve, reject) => {
            axios.post('/admin/shirt/update', formdata)
                .then(response => {
                    console.log(response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)

                    error.response.status == 422 && 
                        commit('SHIRT_ERRORS', error.response.data.errors)

                    reject()
                })
        })
    },

    getShirt: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get(`/admin/shirt/show/${payload.id}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_SHIRT', response.data.shirt)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    getReqShirtData: ({ state, commit }) => {
        return new Promise((resolve, reject) => {
            axios.get('/admin/shirt/get-req-data')
                .then(response => {
                    console.log(response.data);
                    commit('REQ_SHIRT_DATA', response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    reject()
                })
        })
    },

    getShirts: ({ state, commit }, payload) => {

        commit('SHIRT_LOADING', true)
        return new Promise((resolve, reject) => {
            axios.get('/admin/shirt/all?page=' + payload.page)
                .then(response => {
                    console.log(response.data)
                    commit('GET_SHIRTS', response.data)
                    commit('SHIRT_LOADING', false)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    commit('SHIRT_LOADING', false)
                    reject()
                })
        })
    },

    searchShirt: ({ state, commit }, payload) => {

        commit('SHIRT_LOADING', true)
        return new Promise((resolve, reject) => {
            axios.get(`/admin/shirt/all?keywords=${payload.keywords}`)
                .then(response => {
                    console.log(response.data)
                    commit('GET_SHIRTS', response.data)
                    commit('SHIRT_LOADING', false)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    commit('SHIRT_LOADING', false)
                    reject()
                })
        })
    },

    getVariantInfo: ({ state, commit }, payload) => {
        return new Promise((resolve, reject) => {
            axios.get('/admin/shirts/variant/create/' + payload.id)
                .then(response => {
                    console.log(response.data);
                    commit('GET_REQ_VARIANT_DATA', response.data)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    deleteShirt: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)

        return new Promise((resolve, reject) => {
            axios.post('/admin/shirt/delete', formdata)
                .then(response => {
                    console.log(response.data)
                    dispatch('getShirts')
                    resolve()
                })
                .catch(error => {
                    console.log(error.response)
                    reject()
                })
        })
    },

    createVariant: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData();
        formdata.append('shirt_id', payload.shirt_id)
        formdata.append('color_id', payload.color_id)
        formdata.append('size_id', payload.size_id)
        formdata.append('quantity', payload.quantity)

        return new Promise((resolve, reject) => {
            axios.post('/admin/shirts/variant/insert', formdata)
                .then(response => {
                    console.log(response.data);
                    dispatch('getVariants', { id: payload.shirt_id })
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    error.response.status == 422 && 
                        commit('VARIANT_ERRORS', error.response)
                    reject()
                })
        })
    },

    updateVariant: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData();
        formdata.append('id', payload.id)
        formdata.append('shirt_id', payload.shirt_id)
        formdata.append('color_id', payload.color_id)
        formdata.append('size_id', payload.size_id)
        formdata.append('quantity', payload.quantity)

        return new Promise((resolve, reject) => {
            axios.post('/admin/shirts/variant/update', formdata)
                .then(response => {
                    console.log(response.data);
                    dispatch('getVariants', { id: payload.shirt_id })
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    error.response.status == 422 && 
                        commit('VARIANT_ERRORS', error.response)
                    reject()
                })
        })
    },

    getVariants: ({ state, commit }, payload) => {

        return new Promise((resolve, reject) => {
            axios.get('/admin/shirt/variant/all/' + payload.id)
                .then(response => {
                    console.log(response.data);
                    commit('GET_VARIATNS', response.data.variants)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    reject()
                })
        })
    },

    deleteVariant: ({ state, commit, dispatch }, payload) => {
        const formdata = new FormData()
        formdata.append('id', payload.id)

        return new Promise((resolve, reject) => {
            axios.post('/admin/shirts/variant/delete', formdata)
                .then(response => {
                    dispatch('getVariants', { id: payload.shirt_id })
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    reject()
                })
        })
    },

    changeStatus: ({ state, commit, dispatch }, payload) => {
        const formdata = new FormData()
        formdata.append('id', payload.id)
        formdata.append('status', payload.status)

        return new Promise((resolve, reject) => {
            axios.post('/admin/shirts/status/change', formdata)
                .then(response => {
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    reject()
                })
        })
    },
};

const getters = {
    shirtErrors: state => state.shirtErrors,
    getShirts: state => state.shirts,
    getShirt: state => state.shirt,
    shirtLoading: state => state.shirtLoading,
    getReqShirtData: state => state.reqShirtData,
    getReqVariantData: state => state.reqVariantData,
    getReqVariantColor: state => state.reqVariantColor,
    getReqVariantSize: state => state.reqVariantSize,
    getVariants: state => state.variants,
    getVariantErrors: state => state.variantErrors,
};

export default {
    state,
    mutations,
    actions,
    getters
}