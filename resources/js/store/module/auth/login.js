const state = {
    authenticated: localStorage.getItem('authenticated')
            ? localStorage.getItem('authenticated')
            : false,
    adminAuthErrors: null,
    AdminProfileErrors: null,
    getAdmin: null,
    success: null
};

const mutations = {
    AUTH_STATUS: (state, data) => {
        state.authenticated = data
        localStorage.setItem('authenticated', state.authenticated)
    },
    ADMIN_LOGIN_ERRORS: (state, data) => {
        state.adminAuthErrors = data
    },
    ADMIN_PROFILE_ERRORS: (state, data) => {
        state.AdminProfileErrors = data
    },
    ADMIN_LOGOUT: (state) => {
        state.authenticated = false
        localStorage.removeItem("authenticated")
    },
    CLEAR_ERRORS: (state) => {
        state.adminAuthErrors = null;
    },
    GET_ADMIN: (state, data) => {
        state.getAdmin = data
    },
};

const actions = {
    adminLogin: ({ state, dispatch, commit }, payload) => {

        const formdata = new FormData()
        formdata.append('email', payload.email)
        formdata.append('password', payload.password)

        return new Promise((resolve, reject) => {
            axios.post('/admin/login', formdata)
                .then(response => {
                    commit('AUTH_STATUS', true)
                    commit('CLEAR_ERRORS')
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    error.response.status == 422 && 
                        commit('ADMIN_LOGIN_ERRORS', error.response.data.errors)
                    reject()
                })
        })
    },

    adminLogout: ({ state, commit }) => {
        return new Promise((resolve, reject) => {
            axios.post('/admin/logout')
                .then(response => {
                    console.log(response.data);
                    commit('ADMIN_LOGOUT')
                    resolve()                    
                })
                .catch(error => {
                    console.log(error.response);                    
                    reject()
                })
        })
    },

    getAdmin: ({ state, commit }) => {
        return new Promise((resolve, reject) => {
            axios.get('/admin/get-admin')
                .then(response => {
                    console.log(response.data);
                    commit('AUTH_STATUS', response.data.auth)
                    commit('GET_ADMIN', response.data.profile)
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    reject()                    
                })
        })
    },

    updateProfile: ({ state, commit, dispatch }, payload) => {

        const formdata = new FormData()
        formdata.append('id', payload.id)
        formdata.append('name', payload.name)
        formdata.append('email', payload.email)
        
        if (payload.old_password !== '' || payload.new_password !== '') {
            formdata.append('old_password', payload.old_password)
            formdata.append('new_password', payload.new_password)
        }

        return new Promise((resolve, reject) => {
            axios.post('/admin/profile/update', formdata)
                .then(response => {
                    console.log(response.data);
                    resolve()
                })
                .catch(error => {
                    console.log(error.response);
                    error.response.status == 422 && 
                        commit('ADMIN_PROFILE_ERRORS', error.response.data.errors)
                    reject()
                })
        })
    }
};

const getters = {
    adminLoginErrors: state => state.adminAuthErrors,
    adminProfileErrors: state => state.AdminProfileErrors,
    getAdminAuth: state => state.authenticated,
    getAdminProfile: state => state.getAdmin,
};

export default {
    state,
    mutations,
    actions,
    getters
}