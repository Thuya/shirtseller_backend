import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import brand from './module/brand'
import size from './module/size'
import color from './module/color'
import shirt from './module/shirt'
import order from './module/order'
import adminLogin from './module/auth/login'

export const store = new Vuex.Store({
    modules: {
        brand,
        size,
        color,
        shirt,
        order,
        adminLogin,
    }
})